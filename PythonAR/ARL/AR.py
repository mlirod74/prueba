import numpy as np

class ArModel:

    def __init__(self, X, m=10, prtE=80, hz=1):        
        self.X = X
        self.lx = len(X)
        self.m = m
        self.lt = round(self.lx * prtE / 100)
        self.hz = hz
        self.Cf = []        
        self.Xc = []
        self.Yc = []
        self.Ys = []

    def estimateCoefficients(self):
        try:
            if self.lx <  self.lt:
                raise Exception("ERROR: test data does not enought")
            self.X.reverse()
            Yr = []
            Xr = []           
            i = 1
            while i <= self.lt:
                xa = self.lx - (self.m + i + (self.hz-1))
                xb = self.lx - (i - 1)
                Yr.append([self.X[xa]])
                Xr.append(self.X[(xa + 1 + (self.hz-1)):xb])
                i += 1

            Xr = np.mat(Xr)
            Yr = np.mat(Yr) 
            #print("Xr=",np.shape(Xr))
            XrtXr = Xr.transpose() * Xr
            #print("XrtXt=",np.shape(XrtXr))
            XrtXrPI = np.linalg.inv(XrtXr)            
            #print("P=",np.shape(XrtXrPI))
            self.Cf = XrtXrPI * Xr.transpose() * Yr            
        except Exception as e:
            print ("Error fnc estimateCoefficients::",e)

    def testModel(self):
        try:
            if len(self.Cf) == 0:
                raise Exception("ERROR:: Coefficients have not estimated yet")

            
            i = self.lt - (self.hz-1)
            T = self.lx - self.m - 1 - (self.hz-1)
            while i <= T:
                xa = self.lx - (i + self.m + 1 + (self.hz-1))
                xb = self.lx - i
                self.Yc.append([self.X[xa]])
                self.Xc.append(self.X[(xa + 1 + (self.hz-1)):xb])
                i += 1
            self.Xc = np.mat(self.Xc)
            self.Yc = np.mat(self.Yc)
            #print("Len Yc=",len(self.Yc))
            #print("Len Xc=",len(self.Xc))
            self.Ys = self.Xc * self.Cf            
            #print("Len Ys=",len(self.Ys))           
        except Exception as e:
            print ("Error fnc testModel::",e)

    def getCoefficients(self):
        return self.Cf

    def getYc(self):
        return self.Yc

    def getYs(self):
        return self.Ys
