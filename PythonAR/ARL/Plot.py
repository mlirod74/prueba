from matplotlib import pyplot
from pandas import Series



class Plot:

    def showTwoSeries(self, s1, s2, titlePlt, legend1, legend2, xlabel, ylabel):
        serie1 = Series(s1)
        serie1.plot(kind='line', ax=None, figsize=None, use_index=True, title=titlePlt, grid=True, legend=True, label=legend1, linewidth=3)
        serie2 = Series(s2)
        serie2.plot(kind='line', ax=None, figsize=None, use_index=True, title=titlePlt, grid=True, legend=True, label=legend2, linewidth=1)
        pyplot.xlabel(xlabel)
        pyplot.ylabel(ylabel)
        pyplot.show()
