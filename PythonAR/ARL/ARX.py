import numpy as np

class ArxModel:

    def __init__(self, B, A, m=10, prtE=80, hz=1):
        self.B = B
        self.A = A
        self.m = m
        self.lt = round(len(B) * prtE / 100)
        self.hz = hz
        self.Cf = []
        self.lb = len(B)
        self.la = len(A)
        self.Xc = []
        self.Yc = []
        self.Ys = []

    def estimateCoefficients(self):
        try:
            if self.lb <  self.lt:
                raise Exception("ERROR: test data does not enought")
            if self.lb != self.la:
                raise Exception("ERROR: B and A do not have same size")

            self.B.reverse()
            self.A.reverse()
            Yr = []
            Xr = []           
            i = 1
            while i <= self.lt:
                xa = self.la - (self.m + i + (self.hz-1))
                xb = self.la - (i - 1)
                Yr.append([self.A[xa]])
                Xr.append(self.A[(xa + 1 + (self.hz-1)):xb] + self.B[(xa + 1 + (self.hz-1)):xb])
                i += 1

            Xr = np.mat(Xr)
            Yr = np.mat(Yr) 
            XrtXr = Xr.transpose() * Xr
            XrtXrPI = np.linalg.inv(XrtXr)            
            self.Cf = XrtXrPI * Xr.transpose() * Yr    
        except Exception as e:
            print ("Error fnc estimateCoefficients::",e)

    def testModel(self):
        try:
            if len(self.Cf) == 0:
                raise Exception("ERROR:: Coefficients have not estimated yet")

            
            i = self.lt - (self.hz-1)
            T = self.la - self.m - 1 - (self.hz-1)
            while i <= T:
                xa = self.la - (i + self.m + 1 + (self.hz-1))
                xb = self.la - i
                self.Yc.append([self.A[xa]])
                self.Xc.append(self.A[(xa + 1 + (self.hz-1)):xb] + self.B[(xa + 1 + (self.hz-1)):xb])
                i += 1
            self.Xc = np.mat(self.Xc)
            self.Yc = np.mat(self.Yc)
            self.Ys = self.Xc * self.Cf            
        except Exception as e:
            print ("Error fnc testModel::",e)

    def getCoefficients(self):
        return self.Cf

    def getYc(self):
        return self.Yc

    def getYs(self):
        return self.Ys