from AR import ArModel
from ARX import ArxModel
from SVD import SvdHankel
from Plot import Plot
from Metrics import Metrics

#f = open("DataAR.txt", "r")
f = open("Data2.dat", "r")
L = f.readlines()
X = []
for i in range(len(L)):
    X.append(float(L[i].rstrip('\n')))
f.close();


m = 7
h = 5

prtE = 80

#print("X=",X)
print("LEN X=",len(X))
t = round(len(X) * prtE / 100)


Niv = 10
svd = SvdHankel(X,Niv)
Bf, Af = svd.getFrequencies()
#print("Af=",Af)
#print("Bf=",Bf)


#plt1 = Plot()
#plt1.showTwoSeries(Bf, Af, "Frecuencias obtenidas de SVD", "Bajas", "Altas", "Tiempo", "Amplitud")

modelAr = ArModel(Bf.tolist(), m, prtE, h)
modelAr.estimateCoefficients()
modelAr.testModel()
print("LEN AR Cf=",len(modelAr.getCoefficients()))

modelArx = ArxModel(Bf.tolist(), Af.tolist(), m, prtE, h)
modelArx.estimateCoefficients()
modelArx.testModel()
print("LEN ARX Cf=",len(modelArx.getCoefficients()))

Yest = modelAr.getYs().transpose() + modelArx.getYs().transpose()
Ysrc = X[t+m:len(X)]


mtr = Metrics(Ysrc, Yest.A1, m, len(Yest.A1))
print("MSE=",mtr.getMse())
print("RMSE=",mtr.getRmse())
print("MAE=",mtr.getMae())
print("MAPE=",mtr.getMape())
print("MNSE=",mtr.getMnse())
print("R2=",mtr.getR2())
print("GCV=",mtr.getGcv())


plt2 = Plot()
plt2.showTwoSeries(Yest.A1, Ysrc, "Serie de tiempo con SVD, AR y ARX", "Estimado", "Esperado", "Númeno de la muestra (Xi)", "Valor de la muestra (Yi)")

