import numpy as np
import random


class SIDG(object):


    def __init__(self, srX, m=7, h=1, dh=10, maxEpoch=100, srX2=None):
        try:
            self.u = 10**-3
            self.m = m
            self.h = h
            self.dh = dh
            self.maxEpoch = maxEpoch
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Ze = None
            self.Zp = None
            self.V = None
            self.H = None
            self.B = None
            self.dx = 0
            self.srX2 = srX2
            self.__generateM__(srX, m, h, srX2)
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def __generateM__(self, srX, m, h, srX2):
        try:
            te = len(srX)
            print("te:",te)
            srX.reverse()
            i = 1
            limitE = te - m - (h - 1)
            if srX2 is None:
                while i <= limitE:
                    ini = te - (m + i + h - 1)
                    fin = te - (i - 1)
                    self.Ye.append([srX[ini]])
                    self.Xe.append(srX[(ini + h):fin])
                    i += 1
            else:                
                srX2.reverse()
                while i <= limitE:
                    ini = te - (m + i + h - 1)
                    fin = te - (i - 1)
                    self.Ye.append([srX[ini]])
                    self.Xe.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    i += 1

            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.dx = np.shape(self.Xe)[0]
        
            print("Dim Ye:",np.shape(self.Ye))
            print("Dim Xe:",np.shape(self.Xe))
            print("dx:",self.dx)

            self.__initWeights__()

        except Exception as e:
            print ("Error fnc __generateM__::",e)


    def __initWeights__(self):
        try:
            C = 10**8

            self.V = self.__getRandomWeights__()
            print("Init Dim V:",np.shape(self.V))
            self.H = self.__fnActivation__(self.V, self.Xe)
            print("Init Dim H:",np.shape(self.H))
            pinvHj = np.linalg.inv(np.matmul(self.H.transpose(), self.H) + (np.identity(self.dh)/C))
            self.B = np.matmul(np.matmul(pinvHj, self.H.transpose()), self.Ye)
            print("Init Dim B:",np.shape(self.B)) 
            self.Ze = np.matmul(self.H,self.B)
            print("Init Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc __initWeights__::",e)

    def training(self):
        try:
            C = 10**8

            for i in range(self.mlp.maxEpoch):
                GEGV = self.__getGradientDescentWeights__()
                for k in range(self.dh):
                   self.V[k] = self.V[k] - GEGV[k]
                #print("Dim V training:",np.shape(self.V))
                self.H = self.__fnActivation__(self.V, self.Xe)
                #print("Dim H:",np.shape(self.H)) 
                pinvHj = np.linalg.inv(np.matmul(self.H.transpose(), self.H) + (np.identity(self.dh)/C))
                self.B = np.matmul(np.matmul(pinvHj, self.H.transpose()), self.Ye)
                #print("Dim B:",np.shape(self.B)) 
                self.Ze = np.matmul(self.H,self.B)
                #print("Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc training::",e)

    def test(self):
        try:
            H = self.__fnActivation__(self.V, self.Xp)
            print("Dim H:",np.shape(H))
            self.Zp = np.matmul(H,self.B)
            print("Dim Zp:",np.shape(self.Zp))
        except Exception as e:
            print ("Error fnc test::",e)

    def __getRandomWeights__(self):
        a = np.sqrt(6/(self.m + self.dh))
        V = []
        for i in range(self.dh):
            if self.srX2 is None:
                s = np.random.random_sample((self.dx, self.m))
            else:
                s = np.random.random_sample((self.dx, (self.m * 2)))
            V.append(s * 2 * a - a)
        return V 

    def __getGradientDescentWeights__(self):
        drvE = (self.Ye - self.Ze)        
        drvH = (self.H * (1 - self.H)).transpose()
        #print("Dim drvE=",np.shape(drvE))
        #print("Dim drvH=",np.shape(drvH))

        Vi = []
        for i in range(self.dh):
            vp1 = np.matmul(drvE, [self.B[i]])
            #print("Dim vp1=",np.shape(vp1))
            vp2 = np.matmul([drvH[i]], self.Xe)
            #print("Dim vp2=",np.shape(vp2))
            Vi.append(- 1 * self.mlp.u * np.matmul(vp1, vp2))
            #print("Dim Vi=",np.shape(Vi))
        return Vi

    def __fnActivation__(self, V, X):
        H = []
        for i in range(self.dh):
            Vi = V[i]
            Hi = []
            for j in range(len(X)):
                Hi.append(1/(1 + np.exp(-1 * np.dot(X[j], Vi[j]))))
            H.append(Hi)
        H = np.array(H)
        return H.transpose()    

    def getYe(self):
        return self.Ye.transpose()[0]

    def getZe(self):
        return self.Ze.transpose()[0]

    def getYp(self):
        return self.Yp.transpose()[0]

    def getZp(self):
        return self.Zp.transpose()[0]



    def __getRandomWeights2__(self):
        a = np.sqrt(6/(self.m + self.dh))
        if self.srX2 is None:
            s = np.random.random((self.dh, self.m))
        else:
            s = np.random.random((self.dh, (2 * self.m)))
        return ((s * 2 * a) - a)

    def __fnActivation2__(self, V, X):
        return (1/(1 + np.exp(-1 * np.inner(V, X)))).transpose()