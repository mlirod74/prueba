import numpy as np
import random

class SIDG(object):


    def __init__(self, srX,  prtE=80, m=7, h=1, dh=10, maxEpoch=100, srX2=None):
        try:
            self.u = 10**-4
            self.m = m
            self.h = h
            self.dh = dh
            self.maxEpoch = maxEpoch
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Ze = None
            self.Zp = None
            self.V = None
            self.H = None
            self.B = None
            self.dx = 0
            self.srX2 = srX2
            self.__generateM__(srX, prtE, m, h, srX2)
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def __generateM__(self, srX, prtE, m, h, srX2):
        try:
            ts = len(srX)
            te = round(ts * prtE / 100)
            tp = ts - te
            #print("te:",te)
            #print("tp:",tp)
            if srX2 is None:
                srX.reverse()
                i = 1
                limitT = ts - m - (h - 1)
                limitE = te - m - (h - 1)
                while i <= limitT:
                    ini = ts - (m + i + h - 1)
                    fin = ts - (i - 1)
                    if i <= limitE:
                        self.Ye.append([srX[ini]])
                        self.Xe.append(srX[(ini + h):fin])
                    else:
                        self.Yp.append([srX[ini]])
                        self.Xp.append(srX[(ini + h):fin])
                    i += 1
            else:
                srX.reverse()
                srX2.reverse()
                i = 1
                limitT = ts - m - (h - 1)
                limitE = te - m - (h - 1)
                while i <= limitT:
                    ini = ts - (m + i + h - 1)
                    fin = ts - (i - 1)
                    if i <= limitE:
                        self.Ye.append([srX[ini]])
                        self.Xe.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    else:
                        self.Yp.append([srX[ini]])
                        self.Xp.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    i += 1

            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)
            self.dx = np.shape(self.Xe)[0]
        
            #print("Dim Ye:",np.shape(self.Ye))
            #print("Dim Xe:",np.shape(self.Xe))
            #print("Dim Yp:",np.shape(self.Yp))
            #print("Dim Xp:",np.shape(self.Xp))
            #print("dx:",self.dx)

            self.__initWeights__()

        except Exception as e:
            print ("Error fnc __generateM__::",e)


    def __initWeights__(self):
        try:
            C = 10**8

            di = 0
            if self.srX2 is None:
                di = self.m
            else:
                di = 2 * self.m

            self.V = self.__getRandomWeights__(self.dx)  # 7 * 30
            #print("Init Dim V:",np.shape(self.V))
            self.H = self.__fnActivation__(self.V, self.Xe) # 619 * 30
            #print("Init Dim H:",np.shape(self.H))
            pinvHj = np.linalg.inv(np.matmul(self.H.transpose(), self.H) + (np.identity(self.dh)/C)) # 30 * 30
            self.B = np.matmul(np.matmul(pinvHj, self.H.transpose()), self.Ye) # 30 * 1
            #print("Init Dim B:",np.shape(self.B)) 
            self.Ze = np.matmul(self.H,self.B)  # 619 * 1
            #print("Init Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc __initWeights__::",e)

    def __getRandomWeights__(self, dx):
        try:
            a = np.sqrt(6/(self.m + self.dh))
            if self.srX2 is None:
                s = np.random.random_sample((self.m, self.dh))
            else:
                s = np.random.random_sample(((self.m * 2), self.dh))
            return (s * 2 * a - a)
        except Exception as e:
            print ("Error fnc __getRandomWeights__::",e)
        return V 

    def __fnActivation__(self, V, X):
        return 1/(1 + np.exp(-1 * np.matmul(X, V)))  
    
    def training(self):
        try:
            C = 10**8

            di = 0
            if self.srX2 is None:
                di = self.m
            else:
                di = 2 * self.m

            for i in range(self.maxEpoch):
                GEGV = self.__getGradientDescentWeights__()  
                self.V = self.V - GEGV          
                #print("Dim V training:",np.shape(self.V))
                self.H = self.__fnActivation__(self.V, self.Xe) # 619 * 30
                #print("Dim H:",np.shape(self.H)) 
                pinvHj = np.linalg.inv(np.matmul(self.H.transpose(), self.H) + (np.identity(self.dh)/C)) # 30 * 30
                self.B = np.matmul(np.matmul(pinvHj, self.H.transpose()), self.Ye) # 30 * 1
                #print("Dim B:",np.shape(self.B)) 
                self.Ze = np.matmul(self.H,self.B)
                #print("Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc training::",e)

    def __getGradientDescentWeights__(self):
        try:
            drvE = (self.Ye - self.Ze)     
            drvH = (self.H * (1 - self.H)) 
            #print("Dim drvE=",np.shape(drvE))
            #print("Dim drvH=",np.shape(drvH))
            tmp = np.matmul(drvE, self.B.transpose()) 
            tmp = tmp * drvH  # 619 * 30 
            tmp = np.matmul(drvH.transpose(), self.Xe)  
            return (- 1 * self.u * tmp.transpose()) 
        except Exception as e:
            print ("Error fnc __getGradientDescentWeights__::",e)

    def test(self):
        try:
            C = 10**8
            #print("Test Dim V:",np.shape(self.V)) # 7 * 30
            #print("Test Dim Xp:",np.shape(self.Xp)) # 512 * 7 
            #print("Test Dim B:",np.shape(self.B)) # 30 * 1
            H = self.__fnActivation__(self.V, self.Xp) # 512 * 30
            #print("Test Dim H:",np.shape(H))
            self.Zp = np.matmul(H,self.B)  # 512 * 1
            #print("Test Dim Zp:",np.shape(self.Zp))
        except Exception as e:
            print ("Error fnc test::",e)


    def getYe(self):
        return self.Ye.transpose()[0]

    def getZe(self):
        return self.Ze.transpose()[0]

    def getYp(self):
        return self.Yp.transpose()[0]

    def getZp(self):
        return self.Zp.transpose()[0]

