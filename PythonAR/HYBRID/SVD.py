import numpy as np


class SvdHankel:
    def __init__(self, X, nv=3):
        self.X = X
        self.nv = nv

    def __addElementToArray(self, M, E):
        try:
            if np.isscalar(E):
                M.append(E)
            else:
                M.extend(E) 
        except Exception as e:
            print ("Error fnc __addElementToArray::",e)
            raise

    def __getCompFreq(self, Hx):
        try:
            M = []
            if len(Hx) > 1 and len(Hx[0]) > 1:
                E1 = Hx[0,0]
                E2 = (Hx[0,1:] + Hx[1,0:(len(Hx[0])-1)])/2
                E3 = Hx[1,(len(Hx[0])-1)]
                self.__addElementToArray(M, E1)
                self.__addElementToArray(M, E2)
                self.__addElementToArray(M, E3)
            return np.round(M, 12)
        except Exception as e:
            print ("Error fnc __getCompFreq::",e)
            raise

    def getFrequencies(self):
        try:            
            H = [self.X[0:len(self.X)-1],self.X[1:len(self.X)]]            
            #print("H=",H)
            U, S, Vt = np.linalg.svd(H, full_matrices=True)
            #print("U=",U)
            #print("S=",S)
            #print("Vt=",Vt)
            S1 = S[0]
            S2 = S[1]
            Ub = U[:,[0]]
            Ua = U[:,[1]]
            V = Vt.transpose()
            Vb = V[:,[0]].transpose()
            Va = V[:,[1]].transpose()
            Hb = S1 * Ub * Vb
            Ha = S2 * Ua * Va
            #print("Hb=",Hb)
            #print("Ha=",Ha)
            A = self.__getCompFreq(Ha)
            B = self.__getCompFreq(Hb)
            #print("A=",A)
            #print("B=",B)
            self.nv = self.nv -1
            if self.nv > 0:
                _svd = SvdHankel(B,self.nv)
                B, Aj = _svd.getFrequencies()
                #print("Aj=",Aj)
                A = A + Aj
            return B, A
        except Exception as e:
            print ("Error fnc getFrequencies::",e)


#X = [0.2760, 0.6797, 0.6551, 0.1626, 0.1190, 0.4984, 0.9597, 0.3404]
#hm = SvdHankel(X, 3)
#hm.getFrequencies()