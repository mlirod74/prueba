import numpy as np
import random

class SIDG(object):


    def __init__(self, srX,  prtE=80, m=7, h=1, dh=10, maxEpoch=100, srX2=None):
        try:
            self.u = 10**-3
            self.m = m
            self.h = h
            self.dh = dh
            self.maxEpoch = maxEpoch
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Ze = None
            self.Zp = None
            self.V = None
            self.H = None
            self.B = None
            self.dx = 0
            self.srX2 = srX2
            self.__generateM__(srX, prtE, m, h, srX2)
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def __generateM__(self, srX, prtE, m, h, srX2):
        try:
            ts = len(srX)
            te = round(ts * prtE / 100)
            tp = ts - te
            #print("te:",te)
            #print("tp:",tp)
            if srX2 is None:
                srX.reverse()
                i = 1
                limitT = ts - m - (h - 1)
                limitE = te - m - (h - 1)
                while i <= limitT:
                    ini = ts - (m + i + h - 1)
                    fin = ts - (i - 1)
                    if i <= limitE:
                        self.Ye.append([srX[ini]])
                        self.Xe.append(srX[(ini + h):fin])
                    else:
                        self.Yp.append([srX[ini]])
                        self.Xp.append(srX[(ini + h):fin])
                    i += 1
            else:
                srX.reverse()
                srX2.reverse()
                i = 1
                limitT = ts - m - (h - 1)
                limitE = te - m - (h - 1)
                while i <= limitT:
                    ini = ts - (m + i + h - 1)
                    fin = ts - (i - 1)
                    if i <= limitE:
                        self.Ye.append([srX[ini]])
                        self.Xe.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    else:
                        self.Yp.append([srX[ini]])
                        self.Xp.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    i += 1

            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)
            self.dx = np.shape(self.Xe)[0]
        
            #print("Dim Ye:",np.shape(self.Ye))
            #print("Dim Xe:",np.shape(self.Xe))
            #print("Dim Yp:",np.shape(self.Yp))
            #print("Dim Xp:",np.shape(self.Xp))
            #print("dx:",self.dx)

            self.__initWeights__()

        except Exception as e:
            print ("Error fnc __generateM__::",e)


    def __initWeights__(self):
        try:
            C = 10**8

            self.V = self.__getRandomWeights__(self.dx)
            #print("Init Dim V:",np.shape(self.V))
            self.H = self.__fnActivation__(self.V, self.Xe)
            #print("Init Dim H:",np.shape(self.H))
            pinvHj = np.linalg.inv(np.matmul(self.H.transpose(), self.H) + (np.identity(self.dh)/C))
            self.B = np.matmul(np.matmul(pinvHj, self.H.transpose()), self.Ye)
            #print("Init Dim B:",np.shape(self.B)) 
            self.Ze = np.matmul(self.H,self.B)
            #print("Init Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc __initWeights__::",e)

    def __getRandomWeights__(self, dx):
        try:
            a = np.sqrt(6/(self.m + self.dh))
            V = []
            for i in range(self.dh):
                if self.srX2 is None:
                    s = np.random.random_sample((dx, self.m))
                else:
                    s = np.random.random_sample((dx, (self.m * 2)))
                V.append(s * 2 * a - a)
        except Exception as e:
            print ("Error fnc __getRandomWeights__::",e)
        return V 

    def training(self):
        try:
            C = 10**8
            for i in range(self.maxEpoch):
                GEGV = self.__getGradientDescentWeights__()
                for k in range(self.dh):
                   self.V[k] = self.V[k] - GEGV[k]
                #print("Dim V training:",np.shape(self.V))
                self.H = self.__fnActivation__(self.V, self.Xe)
                #print("Dim H:",np.shape(self.H)) 
                pinvHj = np.linalg.inv(np.matmul(self.H.transpose(), self.H) + (np.identity(self.dh)/C))
                self.B = np.matmul(np.matmul(pinvHj, self.H.transpose()), self.Ye)
                #print("Dim B:",np.shape(self.B)) 
                self.Ze = np.matmul(self.H,self.B)
                #print("Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc training::",e)

    def __getGradientDescentWeights__(self):
        try:
            drvE = (self.Ye - self.Ze)        
            drvH = (self.H * (1 - self.H)).transpose()
            #print("Dim drvE=",np.shape(drvE))
            #print("Dim drvH=",np.shape(drvH))
            
            dltV = []
            for i in range(self.dh):
                vp1 = np.matmul(drvE, [self.B[i]])
                #print("Dim vp1=",np.shape(vp1))
                vp2 = np.matmul([drvH[i]], self.Xe)
                #print("Dim vp2=",np.shape(vp2))
                dltV.append(- 1 * self.u * np.matmul(vp1, vp2))
                #print("Dim dltV=",np.shape(dltV))
        except Exception as e:
            print ("Error fnc __getGradientDescentWeights__::",e)
        return dltV

    def test(self):
        try:
            C = 10**8
            print("Test Dim V:",np.shape(self.V))
            print("Test Dim Xp:",np.shape(self.Xp))
            print("Test Dim B:",np.shape(self.B))
            H = self.__fnActivation__(self.V, self.Xp)
            print("Test Dim H:",np.shape(H))
            self.Zp = np.matmul(H,self.B)
            print("Test Dim Zp:",np.shape(self.Zp))
        except Exception as e:
            print ("Error fnc test::",e)



    def __fnActivation__(self, V, X):
        H = []
        for i in range(self.dh):
            Vi = V[i]
            Hi = []
            for j in range(len(X)):
                Hi.append(1/(1 + np.exp(-1 * np.dot(X[j], Vi[j]))))
            H.append(Hi)
        H = np.array(H)
        return H.transpose()    

    def getYe(self):
        return self.Ye.transpose()[0]

    def getZe(self):
        return self.Ze.transpose()[0]

    def getYp(self):
        return self.Yp.transpose()[0]

    def getZp(self):
        return self.Zp.transpose()[0]

