from SIDG import SIDG
from Plot import Plot
from Metrics import Metrics
from SVD import SvdHankel


import numpy as np

#f = open("DataAR.txt", "r")
f = open("Data5.dat", "r")
L = f.readlines()
X = []
for i in range(len(L)):
    X.append(float(L[i].rstrip('\n')))
f.close();

m = 9
h = 4
prtE = 80
dh = 30
u=10**-3
maxEpoch=70
rn = 30
Niv = 4

print("LEN X=",len(X))

svd = SvdHankel(X,Niv)
Bf, Af = svd.getFrequencies()

#for i in range(9, 30, 10): # Rango de nodos ocultos
#    mejorMNSE = 0
#    mejorR2 = 0
#    mejorEpochMNSE = 0
#    mejorEpochR2 = 0
#    tmpMNSE = []
#    tmpR2 = []       
#
#    for j in range(50): # Rango de MAXEPOCH
#    
#        for n in range(rn): # Rango de RUN
#            modelBf = SIDG(Bf.tolist(), prtE, m, h, (i+1), (j+1))
#            modelBf.training()
#            modelBf.test()
#    
#            modelAf = SIDG(Af.tolist(), prtE, m, h, (i+1), (j+1), Bf.tolist())
#            modelAf.training()
#            modelAf.test()
#            Y =  modelBf.getYp() + modelAf.getYp()
#            Z =  modelBf.getZp() + modelAf.getZp()        
#    
#            mtr = Metrics(Y, Z, m, len(Z))
#            tmpMNSE.append(mtr.getMnse())
#            tmpR2.append(mtr.getR2())
#
#        if np.mean(tmpMNSE) > mejorMNSE:
#            mejorMNSE = np.mean(tmpMNSE)
#            mejorEpochMNSE = j+1
#
#        if np.mean(tmpR2) > mejorR2:
#            mejorR2 = np.mean(tmpR2)
#            mejorEpochR2 = j+1
#
#    print("Para ", (i+1)," Nodos, mejorEpochMNSE=", mejorEpochMNSE,", mejorMNSE=", mejorMNSE,", mejorEpochR2=", mejorEpochR2,", mejorR2=",mejorR2)
#    



#NodoMSE = []
#NodoR2 = []
#NodoMNSE = []
#
#NodoNum = []
#ErrorFitness = []
#
#
#promRMSEOld = 0
#promRMSENew = 0
#
#for i in range(dh): # Rango de nodos ocultos
#    tmpMSE = []
#    tmpRMSE = []
#    tmpMAE = []
#    tmpMAPE = []
#    tmpMNSE = []
#    tmpR2 = []
#    
#    for n in range(rn): # Rango de RUN
#        modelBf = SIDG(Bf.tolist(), prtE, m, h, (i+1), maxEpoch)
#        modelBf.training()
#        modelBf.test()
#    
#        modelAf = SIDG(Af.tolist(), prtE, m, h, (i+1), maxEpoch, Bf.tolist())
#        modelAf.training()
#        modelAf.test()
#        #Y =  modelBf.getYp() + modelAf.getYp()
#        #Z =  modelBf.getZp() + modelAf.getZp()        
#        #Y =  modelBf.getYp()
#        #Z =  modelBf.getZp()        
#        Y =  modelAf.getYp()
#        Z =  modelAf.getZp()        
#    
#        mtr = Metrics(Y, Z, m, len(Z))
#        tmpMSE.append(mtr.getMse())
#        tmpRMSE.append(mtr.getRmse())
#        tmpMAE.append(mtr.getMae())
#        tmpMAPE.append(mtr.getMape())
#        tmpMNSE.append(mtr.getMnse())
#        tmpR2.append(mtr.getR2())
#   #print("Para ", (i+1)," nodos Mean MSE=", np.mean(tmpMSE))
#    print("Para ", (i+1)," nodos Mean RMSE=", np.mean(tmpRMSE))
#   #print("Para ", (i+1)," nodos Mean MAE=", np.mean(tmpMAE))
#   #print("Para ", (i+1)," nodos Mean MAPE=", np.mean(tmpMAPE))
#   #print("Para ", (i+1)," nodos Mean MNSE=", np.mean(tmpMNSE))
#   #print("Para ", (i+1)," nodos Mean R2=", np.mean(tmpR2))
#    if promRMSEOld == 0:
#        promRMSEOld = np.mean(tmpRMSE)
#    else:
#        promRMSENew = np.mean(tmpRMSE)
#        ErrorFitness.append(promRMSEOld/promRMSENew)
#        NodoNum.append(i+1)
#        promRMSEOld = promRMSENew 
#   #if i > 10:
#   #    NodoMSE.append(np.mean(tmpMSE))
#   #    NodoR2.append(np.mean(tmpR2))
#   #    NodoNum.append(i+1)
#   #
#
##print("ErrorFitness=", ErrorFitness)
##print("NodoNum=", NodoNum)
#plt1 = Plot()
#plt1.showOneSeries(ErrorFitness, NodoNum, "Entrenamiento Altas Frecuencias", "Error Fitness", "Numero nodos ocultos", "Error Fitness")
#



#tmpMSE = []
#tmpRMSE = []
#tmpMAE = []
#tmpMAPE = []
#tmpMNSE = []
#tmpR2 = []
#tmpGCV = []
#for i in range(dh):
#    modelBf = SIDG(Bf.tolist(), prtE, m, h, dh, maxEpoch)
#    modelBf.training()
#    modelBf.test()
#    
#    modelAf = SIDG(Af.tolist(), prtE, m, h, dh, maxEpoch, Bf.tolist())
#    modelAf.training()
#    modelAf.test()
#    
#    Y =  modelBf.getYp() + modelAf.getYp()
#    Z =  modelBf.getZp() + modelAf.getZp() 
#    
#    #print("Y",Y)
#    #print("Z",Z)
#    
#    mtr = Metrics(Y, Z, m, len(Z))
#    tmpMSE.append(mtr.getMse())
#    tmpRMSE.append(mtr.getRmse())
#    tmpMAE.append(mtr.getMae())
#    tmpMAPE.append(mtr.getMape())
#    tmpMNSE.append(mtr.getMnse())
#    tmpR2.append(mtr.getR2())
#    tmpGCV.append(mtr.getGcv())
#
#print("Mean MSE=", np.mean(tmpMSE))
#print("Mean RMSE=", np.mean(tmpRMSE))
#print("Mean MAE=", np.mean(tmpMAE))
#print("Mean MAPE=", np.mean(tmpMAPE))
#print("Mean MNSE=", np.mean(tmpMNSE))
#print("Mean R2=", np.mean(tmpR2))
#print("Mean GCV=", np.mean(tmpGCV))
#
#plt1 = Plot()
#plt1.showTwoSeries(Y, Z, "Resultados modelo PI+DG", "Y", "Z", "Num Nuestra", "Valor Muesta")



modelBf = SIDG(Bf.tolist(), prtE, m, h, 29, maxEpoch)
modelBf.training()
modelBf.test()

modelAf = SIDG(Af.tolist(), prtE, m, h, 24, maxEpoch, Bf.tolist())
modelAf.training()
modelAf.test()

Y =  modelBf.getYp() + modelAf.getYp()
Z =  modelBf.getZp() + modelAf.getZp() 

mtr = Metrics(Y, Z, m, len(Z))
print("MSE=",mtr.getMse())
print("RMSE=",mtr.getRmse())
print("MAE=",mtr.getMae())
print("MAPE=",mtr.getMape())
print("MNSE=",mtr.getMnse())
print("R2=",mtr.getR2())
print("GCV=",mtr.getGcv())

plt1 = Plot()
plt1.showTwoSeries(Y, Z, "Resultados modelo PI+DG", "Actual Value", "Estimated Value", "Numero Nuestra", "Valor Muestra")


from numpy import ones,vstack
from numpy.linalg import lstsq
points = [(Y[0],Z[0]),(Y[1],Z[1])]
x_coords, y_coords = zip(*points)
A = vstack([x_coords,ones(len(x_coords))]).T
m, c = lstsq(A, y_coords)[0]
funcionLinear = "y = {m}x + {c}".format(m=m,c=c)

import numpy as np
import matplotlib.pyplot as plt

# Create data
x = Z
y = Y

# Plot
plt.scatter(x, y, color='gray', label='Data Points')
plt.legend()
plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)), color='red', label='Best Linear Fit')
plt.legend()
plt.plot(x, x, '-', color='blue', label='Y=X')
plt.legend()
plt.title(funcionLinear)
plt.xlabel('Estimated Value - Target X')
plt.ylabel('Actual Value - Target Y:Linear Fit')
plt.grid()
plt.show()