from EML import EML
from Plot import Plot
from Metrics import Metrics
from SVD import SvdHankel
import numpy as np

#f = open("DataAR.txt", "r")
f = open("Data5.dat", "r")
L = f.readlines()
X = []
for i in range(len(L)):
    X.append(float(L[i].rstrip('\n')))
f.close();

m = 9
h = 4
prtE = 80
dh = 30
rn = 30
Niv = 6

#print("X=",X)
print("LEN X=",len(X))
#print("LEN trainning", t);


svd = SvdHankel(X,Niv)
Bf, Af = svd.getFrequencies()

NodoNum = []
NodoMSE = []
NodoR2 = []

ErrorFitness = []
promRMSEOld = 0
promRMSENew = 0

for i in range(dh):
    tmpMSE = []
    tmpRMSE = []
    tmpMAE = []
    tmpR2 = []
    for n in range(rn):
        modelBf = EML(Bf.tolist(), prtE, m, h, (i+1))
        modelBf.training()
        modelBf.test()

        modelAf = EML(Af.tolist(), prtE, m, h, (i+1), Bf.tolist())
        modelAf.training()
        modelAf.test()
        Y =  modelAf.getYp()# + modelAf.getYp()
        Z =  modelAf.getZp()# + modelAf.getZp()        

        mtr = Metrics(Y, Z, m, len(Z))
        tmpMSE.append(mtr.getMse())
        tmpRMSE.append(mtr.getRmse())
        tmpMAE.append(mtr.getMae())
        tmpR2.append(mtr.getR2())
    #print("Para ", (i+1)," nodos Mean MSE=", np.mean(tmpMSE))
    print("Para ", (i+1)," nodos Mean RMSE=", np.mean(tmpRMSE))
    #print("Para ", (i+1)," nodos Mean MAE=", np.mean(tmpMAE))
    #print("Para ", (i+1)," nodos Mean R2=", np.mean(tmpR2))
    if promRMSEOld == 0:
        promRMSEOld = np.mean(tmpRMSE)
    else:
        promRMSENew = np.mean(tmpRMSE)
        ErrorFitness.append(promRMSEOld/promRMSENew)
        NodoNum.append(i+1)
        promRMSEOld = promRMSENew 

#    if i > 10:
#        NodoMSE.append(np.mean(tmpMSE))
#        NodoR2.append(np.mean(tmpR2))
#        NodoNum.append(i+1)
#
plt1 = Plot()
plt1.showOneSeries(ErrorFitness, NodoNum, "Promedio de MSE por cantidad neuronas capa oculta", "Promedio MSE", "Cantidad Neuronas", "Valor Promedio MSE")


#tmpMSE = []
#tmpRMSE = []
#tmpMAE = []
#tmpMAPE = []
#tmpMNSE = []
#tmpR2 = []
#tmpGCV = []
#for i in range(50):
#    modelBf = EML(Bf.tolist(), prtE, m, h, dh)
#    modelBf.training()
#    modelBf.test()
#    
#    modelAf = EML(Af.tolist(), prtE, m, h, dh, Bf.tolist())
#    modelAf.training()
#    modelAf.test()
#    Y =  modelBf.getYp() + modelAf.getYp()
#    Z =  modelBf.getZp() + modelAf.getZp() 
#    
#    mtr = Metrics(Y, Z, m, len(Z))
#    tmpMSE.append(mtr.getMse())
#    tmpRMSE.append(mtr.getRmse())
#    tmpMAE.append(mtr.getMae())
#    tmpMAPE.append(mtr.getMape())
#    tmpMNSE.append(mtr.getMnse())
#    tmpR2.append(mtr.getR2())
#    tmpGCV.append(mtr.getGcv())
#
#print("Mean MSE=", np.mean(tmpMSE))
#print("Mean RMSE=", np.mean(tmpRMSE))
#print("Mean MAE=", np.mean(tmpMAE))
#print("Mean MAPE=", np.mean(tmpMAPE))
#print("Mean MNSE=", np.mean(tmpMNSE))
#print("Mean R2=", np.mean(tmpR2))
#print("Mean GCV=", np.mean(tmpGCV))



#modelBf = EML(Bf.tolist(), prtE, m, h, dh)
#modelBf.training()
#modelBf.test()
#
#modelAf = EML(Af.tolist(), prtE, m, h, dh, Bf.tolist())
#modelAf.training()
#modelAf.test()
#Y =  modelBf.getYp() + modelAf.getYp()
#Z =  modelBf.getZp() + modelAf.getZp() 
#
#mtr = Metrics(Y, Z, m, len(Z))
#print("MSE=",mtr.getMse())
#print("RMSE=",mtr.getRmse())
#print("MAE=",mtr.getMae())
#print("MAPE=",mtr.getMape())
#print("MNSE=",mtr.getMnse())
#print("R2=",mtr.getR2())
#print("GCV=",mtr.getGcv())
#
#plt1 = Plot()
#plt1.showTwoSeries(Y, Z, "Resultados modelo EML", "Y", "Z", "Num Nuestra", "Valor Muesta")

