import numpy as np
import statistics as st

class Metrics:

    def __init__(self, Yc, Ys, m, t):
        try:
            self.mse = (np.sum(np.square(Yc - Ys)))/(len(Yc))

            self.rmse = np.sqrt(self.mse)

            Er = Yc - Ys
            self.mae = np.sum(np.absolute(Er))/(len(Yc))
            self.mape = np.sum(np.absolute(Er / Yc))/(len(Yc))

            Ycm = np.sum(Yc)/len(Yc)
            Ysm = np.sum(Ys)/len(Ys)
            self.mnse = (1 - np.sum(np.absolute(Er))/np.sum(np.absolute(Yc - Ycm))) * 100            

            Ycr2 = (Yc - np.mean(Yc))/np.std(Yc, ddof=1)
            Ysr2 = (Ys - np.mean(Ys))/np.std(Ys, ddof=1)
            self.R2 = ((np.sum(Ysr2*Ycr2)/(len(Ycr2)-1))**2)*100

            self.gcv = self.mse / np.square((1 - m/len(Yc)))
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def getMse(self):
        return np.round(self.mse,10)

    def getRmse(self):
        return np.round(self.rmse,6)

    def getMae(self):
        return np.round(self.mae,6)

    def getMape(self):
        return np.round(self.mape,6)

    def getMnse(self):
        return np.round(self.mnse,6)

    def getR2(self):
        return np.round(self.R2,6)

    def getGcv(self):
        return np.round(self.gcv,10)