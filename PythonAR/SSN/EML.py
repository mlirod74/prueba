import numpy as np
import random

class EML(object):


    def __init__(self, srX,  prtE=80, m=10, h=1, dh=20, srX2=None):
        try:
            self.m = m
            self.dh = dh
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Ze = None
            self.Zp = None
            self.V = None
            self.B = None
            self.dx = 0
            self.srX2 = srX2
            self.__generateM__(srX, prtE, m, h, srX2)
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def __generateM__(self, srX, prtE, m, h, srX2):
        try:
            ts = len(srX)
            te = round(ts * prtE / 100)
            tp = ts - te
            #print("te:",te)
            #print("tp:",tp)
            if srX2 is None:
                srX.reverse()
                i = 1
                limitT = ts - m - (h - 1)
                limitE = te - m - (h - 1)
                while i <= limitT:
                    ini = ts - (m + i + h - 1)
                    fin = ts - (i - 1)
                    if i <= limitE:
                        self.Ye.append([srX[ini]])
                        self.Xe.append(srX[(ini + h):fin])
                    else:
                        self.Yp.append([srX[ini]])
                        self.Xp.append(srX[(ini + h):fin])
                    i += 1
            else:
                srX.reverse()
                srX2.reverse()
                i = 1
                limitT = ts - m - (h - 1)
                limitE = te - m - (h - 1)
                while i <= limitT:
                    ini = ts - (m + i + h - 1)
                    fin = ts - (i - 1)
                    if i <= limitE:
                        self.Ye.append([srX[ini]])
                        self.Xe.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    else:
                        self.Yp.append([srX[ini]])
                        self.Xp.append(srX[(ini + h):fin] + srX2[(ini + h):fin])
                    i += 1

            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)
            self.dx = np.shape(self.Xe)[0]
        
            #print("Dim Ye:",np.shape(self.Ye))
            #print("Dim Xe:",np.shape(self.Xe))
            #print("Dim Yp:",np.shape(self.Yp))
            #print("Dim Xp:",np.shape(self.Xp))
            #print("dx:",self.dx)
        except Exception as e:
            print ("Error fnc __generateM__::",e)

    def training(self):
        try:
            C = 10**8
            H = None

            self.V = self.__getRandomWeights__()
            #print("Dim V:",np.shape(self.V))
            H = self.__fnActivation__(self.V, self.Xe)
            #print("Dim H:",np.shape(H))
            #print("H:",H)
            pinvHj = np.linalg.inv(np.matmul(H.transpose(), H) + (np.identity(self.dh)/C))
            self.B = np.matmul(np.matmul(pinvHj, H.transpose()), self.Ye)
            #self.B = np.matmul(np.linalg.pinv(H), self.Ye)
            #print("Dim B:",np.shape(self.B)) 
            self.Ze = np.matmul(H,self.B)
            #print("Dim Ze:",np.shape(self.Ze))
        except Exception as e:
            print ("Error fnc training::",e)


    def test(self):
        try:
            H = self.__fnActivation__(self.V, self.Xp)
            #print("Dim H:",np.shape(H))
            self.Zp = np.matmul(H,self.B)
            #print("Dim Zp:",np.shape(self.Zp))
        except Exception as e:
            print ("Error fnc test::",e)

    def __getRandomWeights__(self):
        a = np.sqrt(6/(self.m + self.dh))
        
        if self.srX2 is None:
            s = np.random.random_sample((self.m, self.dh))
        else:
            s = np.random.random_sample(((self.m * 2), self.dh))
        return s * 2 * a - a

    def __fnActivation__(self, V, X):
        return 1/(1 + np.exp(-1 * np.matmul(X, V)))

    def getYe(self):
        return self.Ye.transpose()[0]

    def getZe(self):
        return self.Ze.transpose()[0]

    def getYp(self):
        return self.Yp.transpose()[0]

    def getZp(self):
        return self.Zp.transpose()[0]

    def __getRandomWeights2__(self):
        a = np.sqrt(6/(self.m + self.dh))
        if self.srX2 is None:
            s = np.random.random((self.dh, self.m))
        else:
            s = np.random.random((self.dh, (2 * self.m)))
        return ((s * 2 * a) - a)

    def __fnActivation2__(self, V, X):
        return (1/(1 + np.exp(-1 * np.inner(V, X)))).transpose()