import numpy as np
import random

class EML(object):


    def __init__(self, srX,  prtE=80, m=10, h=1, dh=20, rn=10):
        try:
            self.m = m
            self.dh = dh
            self.rn = rn
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Ze = None
            self.dx = 0
            self.__generateM__(srX, prtE, m, h)
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def __generateM__(self, srX, prtE, m, h):
        try:
            ts = len(srX)
            te = round(ts * prtE / 100)
            tp = ts - te
            print("te:",te)
            print("tp:",tp)
            srX.reverse()
            i = 1
            limitT = ts - m - (h - 1)
            limitE = te - m - (h - 1)
            while i <= limitT:
                ini = ts - (m + i + h - 1)
                fin = ts - (i - 1)
                if i <= limitE:
                    self.Ye.append([srX[ini]])
                    self.Xe.append(srX[(ini + h):fin])
                else:
                    self.Yp.append([srX[ini]])
                    self.Xp.append(srX[(ini + h):fin])
                i += 1
            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)
            self.dx = np.shape(self.Xe)[0]
            print("Dim Ye:",np.shape(self.Ye))
            print("Dim Xe:",np.shape(self.Xe))
            print("Dim Yp:",np.shape(self.Yp))
            print("Dim Xp:",np.shape(self.Xp))
            print("dx:",self.dx)
        except Exception as e:
            print ("Error fnc __generateM__::",e)

    def training(self):
        try:
            C = 10**8
            H = None
            B = None
            for j in range(self.dh):
                Vj = self.__getRandomWeights__()
                print("Dim Vj:",np.shape(Vj))
                Hj = self.__fnActivation__(Vj)
                print("Dim Hj:",np.shape(Hj))
                pinvHj = np.linalg.pinv(np.matmul(Hj.transpose(), Hj) + (np.identity(1)/C))
                Bj = np.matmul(np.matmul(pinvHj, Hj.transpose()), self.Ye)
                print("Dim Bj:",np.shape(Bj))
                if H is None:
                    H = Hj
                else:                    
                    H = np.append(H, Hj, axis=1)
                if B is None:
                    B = Bj
                else:
                    B = np.append(B, Bj, axis=0)                
            print("Dim H:",np.shape(H))
            print("Dim B:",np.shape(B))
            self.Ze = np.matmul(H,B)
        except Exception as e:
            print ("Error fnc training::",e)

    def __getRandomWeights__(self):
        a = np.sqrt(6/(self.m + 1))
        s = np.random.random_sample((self.m, 1))
        return (s * 2 * a - a)*self.dh

    def __fnActivation__(self, Vj):
        return 1/(1 + np.exp(-1 * np.matmul(self.Xe, Vj)))


    def __fnActivationF__(self, V):
        M = [ [ 0 ] * self.dh ] * self.dx        
        a = np.sqrt(6/(self.m + self.dh))
        s = np.random.random_sample((self.dh, self.m))
        s = s * 2 * a - a
        for i in range(self.dx):
            for j in range(self.dh):
                M[i][j] = 1/(1 + np.exp(-1 * np.inner(self.Xe[i,:], s[j,:])))
        return np.array(M)

    def getYe(self):
        return self.Ye.transpose()[0]

    def getZe(self):
        return self.Ze.transpose()[0]