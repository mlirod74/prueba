class Neuron:

    def __init__(self, weights, fnPropagation, fnActivation):
        self.fnPropagation = fnPropagation
        self.fnActivation = fnActivation
        self.weights = weights
        self.net = 0 # Resultado de aplicar la función de transferencia (propagation)
        self.h = 0 # Resultado de aplicar la función de activación
        self.nWeight = len(weights)



