import numpy as np
import random

from Metrics import Metrics
from Plot import Plot

class AEELMBP():

    def __init__(self, X, Y, prtE, nAE=1, nL=1, maxEpoch=100):
        try:
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Hi = []            
            self.nAE = nAE
            self.nL = nL
            self.Wmpl = []
            self.Hmpl = []
            self.maxEpoch = maxEpoch
            self.u = 10**-4
            self.__generateXY__(X, Y, prtE)
            self.zscores = None
            self.accuracy = None
        except Exception as e:
            print ("Error fnc Constructor::",e)


    def __generateXY__(self, X, Y, prtE):
        try:
            ts = np.shape(X)[0] # 2400
            te = round(ts * prtE / 100)
            print("te=",te)

            rndLst = list(range(len(X)))
            random.shuffle(rndLst)
            #print(rndLst)
            
            cnt = 0
            for i in rndLst:
                if cnt < te:
                    self.Ye.append(Y[i]) # (1920, 12)
                    self.Xe.append(X[i]) # (1920, 254)
                else:
                    self.Yp.append(Y[i]) # (480, 12)
                    self.Xp.append(X[i]) # (480, 254)
                cnt += 1 
            
            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)            
        
            print("Dim Ye:",np.shape(self.Ye))
            print("Dim Xe:",np.shape(self.Xe))
            print("Dim Yp:",np.shape(self.Yp))
            print("Dim Xp:",np.shape(self.Xp))

        except Exception as e:
            print ("Error fnc __generateXY__::",e)

    def test(self):
        try:
            print("############## TESTING ##################")
            H = self.Xp.copy().transpose() # (254, 480)
            # Forward on hidden layers
            for i in range(self.nAE):
                Wi = self.Wmpl[i]
                print("Test Dim W[",i,"]=",np.shape(Wi))
                H = self.__fnActivation__(Wi.transpose(), H)                
                print("Test Dim H=", np.shape(H))

            Wclasif = self.Wmpl[self.nAE]
            print("Test  Dim Wclasif=", np.shape(Wclasif))
            Z = self.__fnActivation__(Wclasif.transpose(), H).transpose()

            lZ = Z.transpose().tolist()
            lY = self.Yp.transpose().tolist()
            
            Z = self.__discretizationMax__(Z)             

            print("test Z:",lZ[0][0:10])
            print("test Y:",lY[0][0:10])

            mtr = Metrics(self.Yp.transpose(), Z.transpose(), True)
            print("Accuracies=", mtr.getAccuracies())
            print("Accuracy=", np.mean(mtr.getAccuracies()))
            print("Zcores=", np.round(mtr.getZscores(),4))
            self.zscores = mtr.getZscores()
            self.accuracy = np.mean(mtr.getAccuracies())
        except Exception as e:
            print ("Error fnc test::",e)

    def __discretizationMax__(self, Z):
        try:
            dZf = np.shape(Z)[0] # 480
            dZc = np.shape(Z)[1] # 12
            for i in range(dZf):
                max = np.argmax(Z[i])
                for j in range(dZc):
                    if j == max:
                        Z[i][j] = 1.0
                    else:
                        Z[i][j] = 0.0
            return Z
        except Exception as e:
            print ("Error fnc __discretizationMax__::",e)   

    def __discretizationMin__(self, Z):
        try:
            dZf = np.shape(Z)[0] # 480
            dZc = np.shape(Z)[1] # 12
            for i in range(dZf):
                min = np.argmin(Z[i])
                for j in range(dZc):
                    if j == min:
                        Z[i][j] = 1.0
                    else:
                        Z[i][j] = 0.0
            return Z
        except Exception as e:
            print ("Error fnc __discretizationMin__::",e)   

    def training(self):
        try:
            C = 10**8
            # Generate Auto Encodes
            H = self.Xe.copy().transpose() #(254,1920)
            X = self.Xe.copy().transpose() #(254,1920)
            Y = self.Ye.copy().transpose() #(12,1920)
            self.Hmpl.append(X)
            W = None
            for i in range(self.nAE):
                H, W = self.__generateAE__(H)
                self.Wmpl.append(W)
                self.Hmpl.append(H)
            
            print("Training Dim self.Wmpl=",np.shape(self.Wmpl))
            print("Training Dim self.Hmpl=",np.shape(self.Hmpl))
            self.__backpropagationClasif__(Y)

        except Exception as e:
            print ("Error fnc training::",e)


    def __backpropagationClasif__(self, Y):
        try:
            C = 10**8

            Hclasif = self.Hmpl[self.nAE] # (127, 1920) H of the last AE
            
            MinMse = 1
            
            dYf = np.shape(Y)[0] # 12
            dHf = np.shape(Hclasif)[0] # 127 
            dHc = np.shape(Hclasif)[1] # 1920           
            
            Wclasif = self.__getRandomWeights__(dHf, dYf) # (127, 12)
            #pinvH = np.linalg.inv(np.matmul(Hclasif.transpose(), Hclasif) + (np.identity(dHc)/C)) # (1920, 1920) 
            #Wclasif = np.matmul(Y, np.matmul(pinvH, Hclasif.transpose())).transpose() # (12, 127) 
            self.Wmpl.append(Wclasif)

            #print("__backpropagationClasif__ self.Ye[0]:",self.Ye[0])
            for epoch in range(self.maxEpoch):
                print("ITERATION NUMBER::",(epoch+1))
                Zclasif = self.__fnActivation__(Wclasif.transpose(), Hclasif) # (12, 1920)
                Wclasif = Wclasif.transpose() # (12, 127)

                #delta = []
                #for nout in range(dYf):
                #    deltaNout = (Y[nout] - Zclasif[nout]) * (Zclasif[nout] * (1 - Zclasif[nout])) # (1, 1920)
                #    delta.append(deltaNout)
                #    grdtNout = np.matmul(deltaNout, Hclasif.transpose()) # (1, 127)                    
                #    Wclasif[nout] = Wclasif[nout] + (self.u * grdtNout) # (1, 127)
                #self.Wmpl[self.nAE] = Wclasif.transpose() # (127, 12)
         
                delta = (Y - Zclasif) * (Zclasif * (1 - Zclasif)) #  (12, 1920)
                #print("__backpropagationClasif__ Dim delta:",np.shape(delta))
                grdt = np.matmul(delta, Hclasif.transpose()) # (12, 127)
                #print("__backpropagationClasif__ grdt[0]:",grdt.transpose()[0])
                Wclasif = Wclasif + (self.u * grdt) # (12, 127)
                self.Wmpl[self.nAE] = Wclasif.transpose()

                mtr = Metrics(Y, Zclasif)
                #print("__backpropagationClasif__ MseNodes=", mtr.getMseNodes())
                print("__backpropagationClasif__ Mse=", mtr.getMse())
                if mtr.getMse() < MinMse:
                    MinMse = mtr.getMse()
                else:
                    break
                
                print("delta=",np.shape(delta))
                # Back Proragation for hidden layer
                print("Back Propagation for hidden layer")
                for k in reversed(range(self.nAE)):
                    W = self.Wmpl[k + 1] # (127, 12)
                    V = self.Wmpl[k] # (254, 127)
                    H = self.Hmpl[k + 1] # (127, 1920)
                    X = self.Hmpl[k] # (254,1920)

                    gAgZ = H * (1 - H) # (127, 1920)
                    #print("gAgZ=",np.shape(gAgZ))
                    #print("W=",np.shape(W))
                    delta = np.matmul(W, delta) * gAgZ # (127, 1920)
                    #print("delta=",np.shape(delta))
                    #print("X.transpose()=",np.shape(X.transpose()))
                    grdt = np.matmul(delta, X.transpose()) # (127, 254)
                    #print("grdt=",np.shape(grdt))
                    #print("V=",np.shape(V))
                    V = V + (self.u * grdt.transpose()) # (254, 127)
                    self.Wmpl[k] = V

                    #deltaTmp = []
                    #Vt = V.transpose() # (127, 254)
                    #
                    #for nHn in range(np.shape(H)[0]): # 127 hidden nodes
                    #    gAgZ = H[nHn] * (1 - H[nHn]) # (1, 1920)
                    #    deltaHn = np.matmul(W[nHn], delta) * gAgZ # (1, 1920)
                    #    deltaTmp.append(deltaHn)
                    #    grdt = np.matmul(deltaHn, X.transpose()) # (1, 254)
                    #    Vt[nHn] = Vt[nHn] + (self.u * grdt.transpose()) # (1, 254)
                    #self.Wmpl[k] = Vt.transpose()    
                    #delta = deltaTmp.copy()
              
                # Forward Propagation hidden layers
                print("Forward Propagation hidden layers")
                for k in range (self.nAE):
                    X = self.Hmpl[k]
                    V = self.Wmpl[k]
                    H = self.__fnActivation__(V.transpose(), X) # (127, 1920)
                    self.Hmpl[k+1] = H
                Wclasif = self.Wmpl[self.nAE]
                Hclasif = self.Hmpl[self.nAE]
        except Exception as e:
            print ("Error fnc __backpropagationClasif__::",e)

    def __generateAE__(self, X):
        try:
            C = 10**8
            dXf = np.shape(X)[0] # 254
            dXc = np.shape(X)[1] # 1920
            lowerMse = 100 
            bestNewH = None
            bestW = None
            print("__generateAE__ => Dim X:",np.shape(X))
            for j in range(self.nL):
                dAE =  round(dXf/(2**(j+1)))
                print("dAE=",dAE) # 127

                #V = self.__backpropagationAE__(dAE, X, X)
                V = self.__getRandomWeights__(dXf, dAE) # (254, 127)
                #print("Dim V:",np.shape(V))                               
                H = self.__fnActivation__(V.transpose(), X) # (127, 1920)           
                #print("Dim H:",np.shape(H))
                pinvH = np.linalg.inv(np.matmul(H.transpose(), H) + (np.identity(dXc)/C)) # (1920, 1920) 
                B = np.matmul(X, np.matmul(pinvH, H.transpose())) # (254, 127) 
                #print("Init Dim B:",np.shape(B))
                newH = self.__fnActivation__(B.transpose(), X) # (127, 1920) 
                #print("Init Dim Z:",np.shape(newH))
                Xout = np.matmul(B, H) # (254, 1920)
                
                mtr = Metrics(X, Xout)
                print("__generateAE__ MSE=", mtr.getMse())
                if mtr.getMse() < lowerMse:
                    lowerMse = mtr.getMse()
                    bestNewH = newH.copy()
                    bestW = B.copy()
            #print("__generateAE__ Lowest MSE=",lowerMse)
            #print("__generateAE__ Dim bestNewH=",np.shape(bestNewH))
            #print("__generateAE__ Dim bestW=",np.shape(bestW))
            return bestNewH, bestW
        except Exception as e:
            print ("Error fnc __generateAE__::",e)


    def __backpropagationAE__(self, dAE, X, Y):
        try:
            dXf = np.shape(X)[0] # 254
            dXc = np.shape(X)[1] # 1920
            V = self.__getRandomWeights__(dXf, dAE) # (254, 127)
            H = self.__fnActivation__(V.transpose(), X) # (127, 1920)        
            W = self.__getRandomWeights__(dAE, dXf) # (127, 254)
            Z = self.__fnActivation__(W.transpose(), H) # (254, 1920)
            #Z = np.matmul(W.transpose(), H) # (254, 1920)
            
            for k in range(self.maxEpoch):
                gEgW = (Z - Y) * (Z - Z**2) #  (254, 1920)
                #gEgW = (Z - Y) #  (254, 1920)
                gEgW = np.matmul(gEgW, H.transpose()) # (254, 127)
                W = W + (-1 * self.u * gEgW.transpose()) # (127, 254)

                gEgV = (Z - Y) * (Z - Z**2)  #  (254, 1920)
                #gEgV = (Z - Y)   #  (254, 1920)
                gEgV = np.matmul(gEgV.transpose(), W.transpose()) # (1920, 127)
                gEgV = gEgV * (H - H**2).transpose() # (1920, 127)
                gEgV = np.matmul(gEgV.transpose(), X.transpose()) # (127, 254)
                V = V + (-1 * self.u * gEgV.transpose()) # (254, 127)

                H = self.__fnActivation__(V.transpose(), X) # (127, 1920)        
                Z = self.__fnActivation__(W.transpose(), H) # (254, 1920)


            return V
        except Exception as e:
            print ("Error fnc __backpropagationAE__::",e)

    def __getGradientDescentWeightsAE__(self, X, Y, Z, H, V, B):
        try:
            gEgA = (Z - Y)   # (254,1920)  
            gAgZ = 1
            gEgZ = gEgA * gAgZ  # (254,1920)
            gZgW = H # (127, 1920)
            gEgW = np.matmul(gEgZ, gZgW.transpose()) # (254, 127) 
            B = B + (-1 * self.u * gEgW) # (254, 127)

            gZgA1 = B
            gEgA1 = np.matmul(gEgZ.transpose(), gZgA1) # (1920, 127)  
            gA1gZ1 = (H * (1 - H)) # (127, 1920) 
            gEgZ1 = np.matmul(gEgA1, gA1gZ1) # (1920, 1920)
            gZ1gW1 = X # (254, 1920)
            gEgW1  = np.matmul(gEgZ1, gZ1gW1.transpose()) # (1920, 254)
            V = V + (-1 * self.u * gEgW1) # (254, 127)

            print("Dim drvE=",np.shape(drvE))
            print("Dim drvH=",np.shape(drvH))
            tmp = np.matmul(B.transpose(), drvE) # (127,1920)
            tmp = tmp * drvH  # (127,1920)
            tmp = np.matmul(tmp, X.transpose()) # (127,254)  
            return (- 1 * self.u * tmp.transpose()) # (254, 127)
        except Exception as e:
            print ("Error fnc __getGradientDescentWeights__::",e)

    def __getRandomWeights__(self, dx, dh):
        try:
            a = np.sqrt(6/(dx + dh))
            s = np.random.random_sample((dx, dh))
            return (s * 2 * a - a)
        except Exception as e:
            print ("Error fnc __getRandomWeights__::",e)
        return V 

    def __fnActivation__(self, A, B):
        return 1/(1 + np.exp(-1 * np.matmul(A, B)))  

    def plotZscores(self):
        try:
            plt1 = Plot()
            titlePlot = "Accuracy="+str(round(self.accuracy,4))
            plt1.showOneSeries(self.zscores, list(range(1,len(self.zscores)+1)), titlePlot, "Best F-score", "Class Label", "Best F-score")
        except Exception as e:
            print ("Error fnc plotZscores::",e)