import numpy as np
import random

from Metrics import Metrics
from Plot import Plot

class AEELMPI():

    def __init__(self, X, Y, prtE, nAE=1, nL=1):
        try:
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Hi = []
            self.nAE = nAE
            self.nL = nL
            self.W = []            
            self.__generateXY__(X, Y, prtE)
            self.zscores = None
            self.accuracy  = None
        except Exception as e:
            print ("Error fnc Constructor::",e)


    def __generateXY__(self, X, Y, prtE):
        try:
            ts = np.shape(X)[0] # 2400
            te = round(ts * prtE / 100)
            print("te=",te)

            rndLst = list(range(len(X)))
            random.shuffle(rndLst)
            #print(rndLst)
            
            cnt = 0
            for i in rndLst:
                if cnt < te:
                    self.Ye.append(Y[i]) # (1920, 12)
                    self.Xe.append(X[i]) # (1920, 254)
                else:
                    self.Yp.append(Y[i]) # (480, 12)
                    self.Xp.append(X[i]) # (480, 254)
                cnt += 1 
            
            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)            
        
            print("Dim Ye:",np.shape(self.Ye))
            print("Dim Xe:",np.shape(self.Xe))
            print("Dim Yp:",np.shape(self.Yp))
            print("Dim Xp:",np.shape(self.Xp))

        except Exception as e:
            print ("Error fnc __generateXY__::",e)

    def test(self):
        try:
            print("############## TESTING ##################")
            H = self.Xp.copy().transpose()
            for i in range(len(self.W)):
                Wi = self.W[i]
                print("test Dim W[",i,"]=",np.shape(Wi))
                H = self.__fnActivation__(Wi.transpose(), H)                
                print("test Dim H=", np.shape(H))

            Z = self.__discretization__(H.transpose())    
            print("test Dim Z=", np.shape(Z))

            mtr = Metrics(self.Yp.transpose(), Z.transpose(), True)
            print("test Accuracies=", mtr.getAccuracies())
            print("testAccuracy=", np.mean(mtr.getAccuracies()))
            print("test Zcores=", mtr.getZscores())
            self.zscores = mtr.getZscores()
            self.accuracy = np.mean(mtr.getAccuracies())
        except Exception as e:
            print ("Error fnc test::",e)

    def __discretization__(self, Z):
        try:
            dZf = np.shape(Z)[0] # 480
            dZc = np.shape(Z)[1] # 12
            for i in range(dZf):
                max = np.argmax(Z[i])
                for j in range(dZc):
                    if j == max:
                        Z[i][j] = 1.0
                    else:
                        Z[i][j] = 0.0
            return Z
        except Exception as e:
            print ("Error fnc __discretization__::",e)   

    def training(self):
        try:
            C = 10**8
            # Generate Auto Encodes
            H = self.Xe.copy().transpose() #(254,1920)
            bestW = None
            for i in range(self.nAE):
                H, bestW = self.__generateAE__(H)
                self.W.append(bestW)
            print("Training Dim H:",np.shape(H))
            pinvH = np.linalg.inv(np.matmul(H.transpose(), H) + (np.identity(np.shape(H)[1])/C)) # (1920, 1920) 
            Bclasf = np.matmul(self.Ye.transpose(), np.matmul(pinvH, H.transpose())) # (12, 127)            
            print("Training Dim Bclasf:",np.shape(Bclasf))
            self.W.append(Bclasf.transpose())
        except Exception as e:
            print ("Error fnc training::",e)

    def __generateAE__(self, X):
        try:
            C = 10**8
            dXf = np.shape(X)[0] # 254
            dXc = np.shape(X)[1] # 1920
            lowerMse = 100 
            bestNewH = None
            bestW = None
            print("__generateAE__ => Dim X:",np.shape(X))
            for j in range(self.nL):
                dAE = round(dXf/(2**(j+1)))
                print("dAE=",dAE) # 127
                V = self.__getRandomWeights__(dXf, dAE) # (254, 127)
                print("Dim V:",np.shape(V))
                H = self.__fnActivation__(V.transpose(), X) # (127, 1920)           
                print("Dim H:",np.shape(H))
                pinvH = np.linalg.inv(np.matmul(H.transpose(), H) + (np.identity(dXc)/C)) # (1920, 1920) 
                B = np.matmul(X, np.matmul(pinvH, H.transpose())) # (254, 127) 
                print("Init Dim B:",np.shape(B))
                newH = self.__fnActivation__(B.transpose(), X) # (127, 1920) 
                print("Init Dim newH:",np.shape(newH))
                Xout = np.matmul(B, H) # (254, 1920)

                #print("AAAAAAAAAAAAA X=", X.transpose()[0].tolist())
                #print("BBBBBBBBBBBBB Xout=", Xout.transpose()[0].tolist())

                mtr = Metrics(X, Xout)
                print("MSE=", mtr.getMse())
                if mtr.getMse() < lowerMse:
                    lowerMse = mtr.getMse()
                    bestNewH = newH.copy()
                    bestW = B.copy()
            #print("__generateAE__ lowerMse=",lowerMse)
            #print("__generateAE__ Dim bestNewH=",np.shape(bestNewH))
            #print("__generateAE__ Dim bestW=",np.shape(bestW))
            return bestNewH, bestW
        except Exception as e:
            print ("Error fnc __generateAE__::",e)

    def __getRandomWeights__(self, dx, dh):
        try:
            a = np.sqrt(6/(dx + dh))
            s = np.random.random_sample((dx, dh))
            return (s * 2 * a - a)
        except Exception as e:
            print ("Error fnc __getRandomWeights__::",e)
        return V 

    def __fnActivation__(self, A, B):
        return 1/(1 + np.exp(-1 * np.matmul(A, B)))  

    def plotZscores(self):
        try:
            plt1 = Plot()
            titlePlot = "Accuracy="+str(round(self.accuracy,4))
            plt1.showOneSeries(self.zscores, list(range(1,len(self.zscores)+1)), titlePlot, "Best F-score", "Class Label", "Best F-score")
        except Exception as e:
            print ("Error fnc plotZscores::",e)

