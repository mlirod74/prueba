import numpy as np
from AEELMPI import AEELMPI
from AEELMBP import AEELMBP

from AEELMPI2 import AEELMPI2
from AEELMBP2 import AEELMBP2

from AEELMBI import AEELMBI

X = []
Y = []

#f = open("DataVibration.txt", "r")
#f = open("Xclasif.txt", "r")
f = open("BIClasifX.csv", "r")
L = f.readlines()
for i in range(len(L)):
    Li = np.array(L[i].rstrip('\n').split(",")).astype(float)    
    X.append(Li)
f.close();

#f = open("ClassVibration.txt", "r")
#f = open("Yclasif.txt", "r")
f = open("BIClasifY.csv", "r")
L = f.readlines()
for i in range(len(L)):
    Li = np.array(L[i].rstrip('\n').split(",")).astype(float)    
    Y.append(Li)
f.close();

print("Dim X=",np.shape(X))
print("Dim Y=",np.shape(Y))

prtE = 80
nL = 1
nAE = 1
maxEpoch = 500

#aeelmpi = AEELMPI(X, Y, prtE, nAE, nL)
#aeelmpi.training()
#aeelmpi.test()
#aeelmpi.plotZscores()

#aeelmbp = AEELMBP(X, Y, prtE, nAE, nL, maxEpoch)
#aeelmbp.training()
#aeelmbp.test()
#aeelmbp.plotZscores()

#aeelmpi2 = AEELMPI2(X, Y, prtE, nAE, nL)
#aeelmpi2.training()
#aeelmpi2.test()
#aeelmpi2.plotZscores()


#aeelmbp2 = AEELMBP2(X, Y, prtE, nAE, nL, maxEpoch)
#aeelmbp2.training()
#aeelmbp2.test()
#aeelmbp2.plotZscores()


aeelmbi = AEELMBI(X, Y, prtE, nAE, nL)
aeelmbi.trainingBP()
aeelmbi.testBP()

