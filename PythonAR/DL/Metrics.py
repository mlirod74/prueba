import numpy as np

class Metrics:

    def __init__(self, Y, Z, MC=False):   
        try:                     
            dYf = np.shape(Y)[0] # 12
            dYc = np.shape(Y)[1] # 1920
            self.mseNodes = []

            for k in range(dYf):
                self.mseNodes.append((np.sum(np.square(Y[k] - Z[k])))/dYc)
            self.mse = np.mean(self.mseNodes)

            self.rmse = np.sqrt(self.mse)
           
            
            if MC:
                # Calculate confusion matrix
                self.accurs = []
                self.zscores = []
                self.recalls = []
                self.precisions = []
                self.mc = []
                for i in range(dYf):
                    TP = 0 # True positive
                    TN = 0 # True Negative
                    FP = 0 # False positive
                    FN = 0 # False Negative
                    for j in range(dYc):
                        if Y[i][j] == 1.0 and Z[i][j] == 1.0:
                            TP = TP + 1
                        if Y[i][j] == 0.0 and Z[i][j] == 0.0:
                            TN = TN + 1
                        if Y[i][j] == 1.0 and Z[i][j] == 0.0:
                            FN = FN + 1
                        if Y[i][j] == 0.0 and Z[i][j] == 1.0:
                            FP = FP + 1
                    self.accurs.append(((TP+TN)/(TP+FP+TN+FN))*100)
                    self.zscores.append(((2*TP)/(2*TP + FP + FN))*100)
                    self.recalls.append(TP/(TP+FN)*100)
                    self.precisions.append(TP/(TP+FP)*100)
                    self.mc.append("TP="+str(TP)+", TN="+str(TN)+", FP="+str(FP)+", FN="+str(FN)) 
        except Exception as e:
            print ("Metrics Error fnc Constructor::",e)

    def getMseNodes(self):
        return np.round(self.mseNodes,6)

    def getMse(self):
        return np.round(self.mse,6)

    def getRmse(self):
        return np.round(self.rmse,6)

    def getAccuracies(self):
        return self.accurs

    def getZscores(self):
        return self.zscores

    def getRecalls(self):
        return self.recalls

    def getPrecisions(self):
        return self.precisions

    def getMC(self):
        return self.mc