import numpy as np
import random
import math

from Metrics import Metrics
from Plot import Plot

class AEELMBI():

    def __init__(self, X, Y, prtE, nAE=1, nL=1):
        try:
            self.Ye = []
            self.Xe = []
            self.Yp = []
            self.Xp = []
            self.Hi = []
            self.nAE = nAE
            self.nL = nL
            self.W = []     
            self.Wmpl = []
            self.Hmpl = []    
            self.u = 10**-4
            self.__generateXY__(X, Y, prtE)
            self.zscores = None
            self.accuracy  = None
        except Exception as e:
            print ("Error fnc Constructor::",e)

    def __generateXY__(self, X, Y, prtE):
        try:
            ts = np.shape(X)[0] 
            te = round(ts * prtE / 100)
            print("te=",te)

            rndLst = list(range(len(X)))
            random.shuffle(rndLst)
            
            cnt = 0
            for i in rndLst:
                if cnt < te:
                    self.Ye.append(Y[i]) 
                    self.Xe.append(X[i]) 
                else:
                    self.Yp.append(Y[i]) 
                    self.Xp.append(X[i]) 
                cnt += 1 
            
            self.Ye = np.array(self.Ye)
            self.Xe = np.array(self.Xe)
            self.Yp = np.array(self.Yp)
            self.Xp = np.array(self.Xp)            
        
            print("Dim Ye:",np.shape(self.Ye))
            print("Dim Xe:",np.shape(self.Xe))
            print("Dim Yp:",np.shape(self.Yp))
            print("Dim Xp:",np.shape(self.Xp))

        except Exception as e:
            print ("Error fnc __generateXY__::",e)

    def test(self):
        try:
            print("############## TESTING ##################")
            H = self.Xp.copy().transpose()
            for i in range(len(self.W)):
                Wi = self.W[i]
                print("test Dim W[",i,"]=",np.shape(Wi))
                H = self.__fnActivation__(Wi.transpose(), H)                
                print("test Dim H=", np.shape(H))
    
            listZ = H.tolist()
            listY = self.Yp.transpose().copy().astype(int).tolist()
    
            print("test List Z=", np.round(listZ[0][0:100],4))
            print("test List Y=", listY[0][0:100])
    
            Z = self.__discretization__(H.transpose())    
    
            #print("test Z=", Z.transpose()[0][0:100])
    
            print("test Z0=",len(Z[Z == 0])," Z1=",len(Z[Z == 1]))
            print("test Y0=",len(self.Yp[self.Yp == 0])," Y1=",len(self.Yp[self.Yp == 1]))
    
            mtr = Metrics(self.Yp.transpose(), Z.transpose(), True)
            print("test MC::", mtr.getMC())
            print("test Accuracy=", np.round(np.mean(mtr.getAccuracies()),4))
            print("test Recall=", np.round(np.mean(mtr.getRecalls()),4))
            print("test Precision=", np.round(np.mean(mtr.getPrecisions()),4))
            print("test Zcore=", np.round(np.mean(mtr.getZscores()),4))
        except Exception as e:
            print ("Error fnc test::",e)

    def __discretization__(self, Z):
        try:
            dZf = np.shape(Z)[0] 
            dZc = np.shape(Z)[1] 
            print("__discretization__ dZf=", dZf)
            print("__discretization__ dZc=", dZc)
            limit = round(np.mean(Z),1) - 0.1
            print("__discretization__ limit=", limit)
            for i in range(dZf):
                for j in range(dZc):
                    if Z[i][j] > limit:
                        Z[i][j] = 1.0
                    else:
                        Z[i][j] = 0.0
            return Z
        except Exception as e:
            print ("Error fnc __discretization__::",e)   

    def training(self):
        try:
            C = 10**4
            # Generate Auto Encodes
            H = self.Xe.copy().transpose() 
            Y = self.Ye.copy().transpose()
            bestW = None
            for i in range(self.nAE):
                H, bestW = self.__generateAE__(H)
                self.W.append(bestW)
    
            print("Training Dim H:",np.shape(H))
            pinvH = np.linalg.inv(np.matmul(H.transpose(), H) + (np.identity(np.shape(H)[1])/C)) 
            Bclasf = np.matmul(self.Ye.transpose(), np.matmul(pinvH, H.transpose()))            
            print("Training Dim Bclasf:",np.shape(Bclasf))
            self.W.append(Bclasf.transpose())
        except Exception as e:
            print ("Error fnc training::",e)

    def __generateAE__(self, X):
        try:
            C = 10**8
            dXf = np.shape(X)[0] 
            dXc = np.shape(X)[1] 
            lowerMse = 100 
            bestNewH = None
            bestW = None
            print("__generateAE__ => Dim X:",np.shape(X))
            
            dAE = 100 #round(dXf/(2**(j+1)))
            print("dAE=",dAE) 
            V = self.__getRandomWeights__(dXf, dAE) 
            print("Dim V:",np.shape(V))
            H = self.__fnActivation__(V.transpose(), X) 
            print("Dim H:",np.shape(H))
            pinvH = np.linalg.inv(np.matmul(H.transpose(), H) + (np.identity(dXc)/C)) 
            B = np.matmul(X, np.matmul(pinvH, H.transpose())) 
            print("Init Dim B:",np.shape(B))
            newH = self.__fnActivation__(B.transpose(), X) 
            print("Init Dim newH:",np.shape(newH))
            Xout = np.matmul(B, H) 

            mtr = Metrics(X, Xout)
            print("MSE=", mtr.getMse())
            if mtr.getMse() < lowerMse:
                lowerMse = mtr.getMse()
                bestNewH = newH.copy()
                bestW = B.copy()
            return bestNewH, bestW
        except Exception as e:
            print ("Error fnc __generateAE__::",e)

    def __getRandomWeights__(self, dx, dh):
        try:
            a = np.sqrt(6/(dx + dh))
            s = np.random.random_sample((dx, dh))
            return (s * 2 * a - a)
        except Exception as e:
            print ("Error fnc __getRandomWeights__::",e)
        return V 

    def __fnActivation__(self, A, B):
        return 1/(1 + np.exp(-1 * np.matmul(A, B)))  

    def plotZscores(self):
        try:
            plt1 = Plot()
            titlePlot = "Accuracy="+str(round(self.accuracy,4))
            plt1.showOneSeries(self.zscores, list(range(1,len(self.zscores)+1)), titlePlot, "Best F-score", "Class Label", "Best F-score")
        except Exception as e:
            print ("Error fnc plotZscores::",e)


    def testBP(self):
        try:
            print("############## TESTING ##################")
            H = self.Xp.copy().transpose() # (254, 480)
            # Forward on hidden layers
            for i in range(self.nAE):
                Wi = self.Wmpl[i]
                print("Test Dim W[",i,"]=",np.shape(Wi))
                H = self.__fnActivation__(Wi.transpose(), H)                
                print("Test Dim H=", np.shape(H))

            Wclasif = self.Wmpl[self.nAE]
            print("Test  Dim Wclasif=", np.shape(Wclasif))

            Z = self.__fnActivation__(Wclasif.transpose(), H).transpose()

            listZ = Z.tolist()
            listY = self.Yp.transpose().tolist()

            print("test List Z=", listZ[0][0:100])
            print("test List Y=", listY[0][0:100])
            
            Z = self.__discretization__(Z)                        

            print("test Z0=",len(Z[Z == 0])," Z1=",len(Z[Z == 1]))
            print("test Y0=",len(self.Yp[self.Yp == 0])," Y1=",len(self.Yp[self.Yp == 1]))

            mtr = Metrics(self.Yp.transpose(), Z.transpose(), True)
            print("test MC::", mtr.getMC())
            print("test Accuracy=", np.round(np.mean(mtr.getAccuracies()),4))
            print("test Recall=", np.round(np.mean(mtr.getRecalls()),4))
            print("test Precision=", np.round(np.mean(mtr.getPrecisions()),4))
            print("test Zcore=", np.round(np.mean(mtr.getZscores()),4))    
        except Exception as e:
            print ("Error fnc test::",e)


    def trainingBP(self):
        try:
            C = 10**8
            # Generate Auto Encodes
            H = self.Xe.copy().transpose() #(254,1920)
            X = self.Xe.copy().transpose() #(254,1920)
            Y = self.Ye.copy().transpose() #(12,1920)
            self.Hmpl.append(X)
            W = None
            for i in range(self.nAE):
                H, W = self.__generateAE__(H)
                self.Wmpl.append(W)
                self.Hmpl.append(H)
            
            print("Training Dim self.Wmpl=",np.shape(self.Wmpl))
            print("Training Dim self.Hmpl=",np.shape(self.Hmpl))
            self.__backpropagationClasif__(Y)

        except Exception as e:
            print ("Error fnc training::",e)

    def __backpropagationClasif__(self, Y):
        try:
            C = 10**8

            Hclasif = self.Hmpl[self.nAE] # (127, 1920) H of the last AE            
            MinMse = 1
            
            dYf = np.shape(Y)[0] # 12
            dHf = np.shape(Hclasif)[0] # 127 
            dHc = np.shape(Hclasif)[1] # 1920           
            
            Wclasif = self.__getRandomWeights__(dHf, dYf) # (127, 12)
            #pinvH = np.linalg.inv(np.matmul(Hclasif.transpose(), Hclasif) + (np.identity(dHc)/C)) # (1920, 1920) 
            #Wclasif = np.matmul(Y, np.matmul(pinvH, Hclasif.transpose())).transpose() # (12, 127) 
            self.Wmpl.append(Wclasif)

            #print("__backpropagationClasif__ self.Ye[0]:",self.Ye[0])
            for epoch in range(500):
                print("ITERATION NUMBER::",(epoch+1))
                Zclasif = self.__fnActivation__(Wclasif.transpose(), Hclasif) # (12, 1920)
                Wclasif = Wclasif.transpose() # (12, 127)
         
                delta = (Y - Zclasif) * (Zclasif * (1 - Zclasif)) #  (12, 1920)
                #print("__backpropagationClasif__ Dim delta:",np.shape(delta))
                grdt = np.matmul(delta, Hclasif.transpose()) # (12, 127)
                #print("__backpropagationClasif__ grdt[0]:",grdt.transpose()[0])
                Wclasif = Wclasif + (self.u * grdt) # (12, 127)
                self.Wmpl[self.nAE] = Wclasif.transpose()

                mtr = Metrics(Y, Zclasif)
                #print("__backpropagationClasif__ MseNodes=", mtr.getMseNodes())
                print("__backpropagationClasif__ Mse=", mtr.getMse())
                if mtr.getMse() < MinMse:
                    MinMse = mtr.getMse()
                else:
                    break
                
                print("delta=",np.shape(delta))
                # Back Proragation for hidden layer
                print("Back Propagation for hidden layer")
                for k in reversed(range(self.nAE)):
                    W = self.Wmpl[k + 1] # (127, 12)
                    V = self.Wmpl[k] # (254, 127)
                    H = self.Hmpl[k + 1] # (127, 1920)
                    X = self.Hmpl[k] # (254,1920)

                    gAgZ = H * (1 - H) # (127, 1920)
                    #print("gAgZ=",np.shape(gAgZ))
                    delta = np.matmul(W, delta) * gAgZ # (127, 1920)
                    #print("delta=",np.shape(delta))
                    grdt = np.matmul(delta, X.transpose()) # (127, 254)
                    #print("grdt=",np.shape(grdt))
                    V = V + (self.u * grdt.transpose()) # (254, 127)
                    self.Wmpl[k] = V
              
                # Forward Propagation hidden layers
                print("Forward Propagation hidden layers")
                for k in range (self.nAE):
                    X = self.Hmpl[k]
                    V = self.Wmpl[k]
                    H = self.__fnActivation__(V.transpose(), X) # (127, 1920)
                    self.Hmpl[k+1] = H
                Wclasif = self.Wmpl[self.nAE]
                Hclasif = self.Hmpl[self.nAE]
        except Exception as e:
            print ("Error fnc __backpropagationClasif__::",e)
